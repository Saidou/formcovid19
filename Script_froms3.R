
rm(list = ls())

# 1. Enter the name of signer and the name of the database ---------------------
## The authorized signatories are: Rodrigue BIKANGUI / Georgelin NGUEMA ONDO / Prof Ayola Akim ADEGNIKA / Prof Bertrand LELL

chef_lab <- "Rodrigue BIKANGUI" 

path_xlsx <- "data/Results 29 November 2021.xlsx"

sheet_name <- "General"

date <- format(Sys.Date(), "%d-%m-%Y")

# date <- "20-11-2021"

# 2. Libraries -----------------------------------------------------------------
library(dplyr)
library(readxl)
library(stringr)
library(lubridate)

# 3. Importing and manupulation data -------------------------------------------
database <- read_excel(path_xlsx, sheet = sheet_name)

names(database) <- c("Id_lab", "nom_prenom", "Id_patient", "sexe", "age_saisi", "date_birth", "phone", "residance",
                     "status_vac", "ville", "date_recep", "heure_recep", "date_diagn", "heure_diagn", "provenance",
                     "date_colect", "heure_colect", "contexte", "prelev_j", "nbr_ech", "ct_1", "ct_2","comment",
                     "conclusion", "gene_1", "gene_2", "kit", "methode", "equipment", "type_ecouv", "type_test")

data <- database %>%
  mutate_at(vars(starts_with("ct_")), ~(as.numeric(gsub("Ind", NA, .)))) %>%
  mutate(age_saisi = as.numeric(str_extract(age_saisi, "^\\d+")),
         age = floor(decimal_date(date_diagn) - decimal_date(date_birth)),
         age = if_else(is.na(age), as.numeric(age_saisi), age),
         Id_patient = as.character(Id_patient),
         type_test = str_to_upper(type_test),
         type_test = if_else(type_test == "TDR", "TDR", NA_character_),
         provenance = case_when(str_detect(str_to_lower(provenance), "prom") ~ "Maurel et Prom",
                                is.na(provenance) ~ "Autres",
                                TRUE ~ provenance),
         prelev_j = case_when(str_detect(str_to_lower(prelev_j), "ns") ~ NA_character_,
                              is.na(prelev_j) ~ NA_character_,
                              str_detect(str_to_upper(prelev_j), "J0") ~ "J0 (Diagnostic initial)",
                              TRUE ~ paste(prelev_j, "(suivi/follow-up)")),
         res_gene_1 = if_else(is.na(ct_1), "non détecté/not detected", "détecté/detected"),
         res_gene_2 = if_else(is.na(ct_2), "non détecté/not detected", "détecté/detected"),
         res_tdr = if_else(str_detect(str_to_lower(conclusion), "positi") & !is.na(type_test), "détecté/detected", "non détecté/not detected"),
         conclusion = case_when(str_detect(str_to_lower(conclusion), "positif") ~ "Positif",
                             str_detect(str_to_lower(conclusion), "gatif") ~ "Negatif",
                             str_detect(str_to_lower(conclusion), "ind") ~ "Indéterminé",
                             TRUE ~ "Autres"),
         comment = case_when(str_detect(conclusion, "ositif") ~ "Infection au Sars-CoV-2 détectée/detected Sars-CoV-2 infection",
                             str_detect(conclusion, "gatif") ~ "Infection au Sars-CoV-2 non détectée/Undetected Sars-CoV-2 infection",
                             str_detect(conclusion, "Ind") ~ "Indéterminé",
                             TRUE ~ "Invalide / erreur"),
         interpret = case_when(comment == "Infection au Sars-CoV-2 détectée/detected Sars-CoV-2 infection" ~ "Positif/Positive",
                               comment == "Infection au Sars-CoV-2 non détectée/Undetected Sars-CoV-2 infection" ~ "Négatif/Negative",
                               TRUE ~ comment),
         signe_by = chef_lab,
         signature = paste0("sig_", str_to_lower(str_replace_all(signe_by, c(" " = "_"))), "_stamp.jpg")) %>%
  mutate_at(vars(contains("date")), ~(format(., "%d-%m-%Y"))) %>%
  arrange(provenance)

# 4. Render --------------------------------------------------------------------

filter_day <- max(data$date_diagn)

## 4.1. files reports and code -------------------------------------------------
reportfolder <- "reports/"
filereportsig <- "forms_parent_sig.Rmd"
filereportnos <- "forms_parent_nos.Rmd"

## 4.2. Report all -------------------------------------------------------------
outpath_sig <- paste0(reportfolder, "/Resultat_du_", filter_day, "_signed")
outpath_nos <- paste0(reportfolder, "/Resultat_du_", filter_day, "_nosigned")
rmarkdown::render(filereportsig, output_file = outpath_sig, quiet = TRUE)
rmarkdown::render(filereportnos, output_file = outpath_nos, quiet = TRUE)

