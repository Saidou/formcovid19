---
output: 
  pdf_document: 
    number_sections: yes
---
\begin{picture}(0, 0)(0, 0)
\includegraphics[height = 2.25 cm]{logo/logo_cermel.png}
\end{picture}
\begin{picture}(0,0)(-215, 25)
\includegraphics[height = 3.25 cm]{logo/logo-minist.png}
\end{picture}
\vspace{1cm}
\begin{center}
\large{\textbf{RESULTAT DU TEST DE DIAGNOSTIC SARS-CoV-2 (COVID-19)}}\\
\textbf{\textcolor{gray}{SARS-CoV-2 DIAGNOSIS TEST RESULT (COVID-19)}}
\end{center}
\vspace{0.25cm}

```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
options(knitr.kable.NA = "")

data_child <- data_parent %>%
  slice(j)
```


\hspace{0.25cm}{\textbf{Patient}}\
\vspace{-1.0cm}

\hspace{0.12cm}
\begin{tabular}{|>{\columncolor{gris1}}p{3.7cm}|p{13.35cm}|}
\hline
Nom et prénom: & \multirow{2}*{`r data_child$nom_prenom`}\\[-0.06cm]
\textcolor{gray}{\footnotesize{Family name, given name}} &\\[-0.04cm]
\hline
\end{tabular}


\begin{tabular}{p{9.0cm}p{9.0cm}}
        \begin{tabular}{|>{\columncolor{gris1}}p{3.7cm}|p{4.0cm}l|}
        \hline
        ID Laboratoire & \multirow{2}*{`r data_child$Id_lab`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Lab ID}} & &  \\[-0.04cm]
        \hline
        Date de naissance & \multirow{2}*{`r ifelse(is.na(data_child$date_birth), " ", data_child$date_birth)`} &\\[-0.06cm]
        \textcolor{gray}{\footnotesize{Date of birth}} & & \\[-0.04cm]
        \hline
        Sexe & \multirow{2}*{`r ifelse(is.na(data_child$sexe), " ", data_child$sexe)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Sex}} & & \\[-0.04cm]
        \hline
        Adresse & \multirow{2}*{`r ifelse(is.na(data_child$residance), " ", data_child$residance)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Adress}} & & \\[-0.04cm]
        \hline
        \end{tabular}
        &
        \begin{tabular}{|>{\columncolor{gris1}}p{3.2cm}|p{4cm}l|}
        \hline
        ID patient & \multirow{2}*{`r ifelse(is.na(data_child$Id_patient), " ", data_child$Id_patient)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{ID Patient}} & & \\[-0.04cm]
        \hline
        Age (ans) & \multirow{2}*{`r ifelse(is.na(data_child$age), " ", data_child$age)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Age (years)}} & & \\[-0.04cm]
        \hline
        Phone & \multirow{2}*{`r ifelse(is.na(data_child$phone), " ", data_child$phone)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Phone}} & & \\[-0.04cm]
        \hline
        Ville & \multirow{2}*{`r ifelse(is.na(data_child$ville), " ", data_child$ville)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Town}} & & \\[-0.04cm]
        \hline
        \end{tabular}
   \end{tabular}

\vspace{0.25cm}
\hspace{0.25cm}{\textbf{Prelevement}}\
\vspace{-1cm}
\begin{tabular}{p{9.0cm}p{9.0cm}}
        \begin{tabular}{|>{\columncolor{gris1}}p{3.7cm}|p{4.0cm}l|}
        \hline
        \footnotesize{Date et heure du prélèvement} & \multirow{2}*{`r paste(ifelse(is.na(data_child$date_colect), " ", data_child$date_colect), ifelse(is.na(data_child$heure_colect), " ", data_child$heure_colect), sep = "  ")`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Date and Time of sampling}} & &  \\[-0.04cm]
        \hline
        Date de réception & \multirow{2}*{`r paste(ifelse(is.na(data_child$date_recep), " ", data_child$date_recep), ifelse(is.na(data_child$heure_recep), " ", data_child$heure_recep), sep = "  ")`} &\\[-0.06cm]
        \textcolor{gray}{\footnotesize{Date of reception }} & & \\[-0.04cm]
        \hline
        Contexte & \multirow{2}*{`r ifelse(is.na(data_child$contexte), " ", data_child$contexte)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Context}} & & \\[-0.04cm]
        \hline
        \end{tabular}
        &
        \begin{tabular}{|>{\columncolor{gris1}}p{3.2cm}|p{4cm}l|}
        \hline
        Lieu de prélèvement & \multirow{2}*{`r data_child$provenance`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Place of sampling}} & & \\[-0.04cm]
        \hline
        Type de prélèvement & \multirow{2}*{`r ifelse(is.na(data_child$type_ecouv), " ", data_child$type_ecouv)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Type of sample}} & & \\[-0.04cm]
        \hline
        Jour de suivi & \multirow{2}*{`r ifelse(is.na(data_child$prelev_j), " ", data_child$prelev_j)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Time point}} & & \\[-0.04cm]
        \hline
        \end{tabular}
\end{tabular}

\vspace{0.25cm}
\hspace{0.25cm}{\textbf{Analyse}}\
\vspace{-1cm}
\begin{tabular}{p{9.0cm}p{9.0cm}}
        \begin{tabular}{|>{\columncolor{gris1}}p{3.7cm}|p{4.0cm}l|}
        \hline
        Date d’analyse & \multirow{2}*{`r paste(data_child$date_diagn, ifelse(is.na(data_child$heure_diagn), " ", data_child$heure_diagn), sep = "  ")`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Date of analysis}} & &  \\[-0.04cm]
        \hline
        Equipement & \multirow{2}*{`r ifelse(is.na(data_child$equipment), " ", data_child$equipment)`} &\\[-0.06cm]
        \textcolor{gray}{\footnotesize{Equipment }} & & \\[-0.04cm]
        \hline
        \end{tabular}
        &
        \begin{tabular}{|>{\columncolor{gris1}}p{3.2cm}|p{4cm}l|}
        \hline
        Methode & \multirow{2}*{`r ifelse(is.na(data_child$methode), " ", data_child$methode)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Method}} & & \\[-0.04cm]
        \hline
        Kit & \multirow{2}*{`r ifelse(is.na(data_child$kit), " ", data_child$kit)`} & \\[-0.06cm]
        \textcolor{gray}{\footnotesize{Kit}} & & \\[-0.04cm]
        \hline
        \end{tabular}
\end{tabular}

\vspace{0.25cm}
\hspace{0.25cm}{\textbf{Results}}\
\vspace{-1cm}
\begin{tabular}{p{8.0cm}p{8.0cm}}
      \begin{tabular}{|p{3.7cm}|p{4.0cm}|p{8.98cm}|}
      \hline
      \cellcolor{gris1}\textbf{`r ifelse(is.na(data_child$type_test), "Gene", "Type")`} & \cellcolor{gris1}\textbf{Sars-CoV-2} & \cellcolor{gris1}\textbf{Conclusion} \\
      \hline
      \multirow{2}*{`r ifelse(!is.na(data_child$gene_1), data_child$gene_1, ifelse(is.na(data_child$type_test), " ", data_child$type_test))`} & \multirow{2}*{`r ifelse(!is.na(data_child$gene_1), data_child$res_gene_1, ifelse(is.na(data_child$type_test), "non détecté", data_child$res_tdr))`} &  \multirow{4}*{\footnotesize{`r data_child$comment`}}\\[0.0cm]
      & & \\
      \cline{1-2}
      \multirow{2}*{`r ifelse(is.na(data_child$gene_2), " ", data_child$gene_2)`} & \multirow{2}*{`r ifelse(!is.na(data_child$gene_2), data_child$res_gene_2, " ")`} & \\[-0.12cm]
      & & \\
      \hline
      \end{tabular}
\end{tabular}

\vspace{0.25cm}
\hspace{0.25cm}{\textbf{Interpretation:}}\
\vspace{-1cm}

\hspace{0.25cm}{`r data_child$interpret`}

```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE, results='hide'}
k <- data_child$Id_lab

patient_id <- data_child %>% filter(Id_lab == k) %>% pull(Id_lab)
patient_name <- data_child %>% filter(Id_lab == k) %>% pull(nom_prenom)
patient_residence <- data_child %>% filter(Id_lab == k) %>% pull(residance)
patient_result <- data_child %>% filter(Id_lab == k) %>% pull(conclusion)
date_diagnostic <- data_child %>% filter(Id_lab == k) %>% pull(date_diagn)
date_naiss <- data_child %>% filter(Id_lab == k) %>% pull(date_birth)

code <- paste("Laboratoire: CERMEL", paste("Id Lab:", patient_id), paste("Nom:", patient_name),
              paste("Date Naiss:", date_naiss), paste("Resultat:", patient_result),
              paste("Date resultat:", date_diagnostic), sep = "\n")
cat(code)
```


```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
png(filename = paste0("qrcode/", k, ".png"))
qrcode_gen(code)
invisible(dev.off())
```


```{r echo = FALSE, results='asis'}
cat(c("\\hspace{0.35cm}\\vspace{-0.5cm}
\\begin{table}[!htb]
\\begin{minipage}{.6\\linewidth}
    \\begin{flushleft}
        \\textbf{} \\newline 
        \\textnormal{} \\newline 
        \\includegraphics[height = 4.2cm]{", paste0("qrcode/", k, ".png"), "}\\newline
        \\textbf{} \\newline
    \\end{flushleft}
    \\end{minipage}%
    \\begin{minipage}{.6\\linewidth}
    \\begin{flushleft}\\vspace{-0.5cm}
        \\textnormal{Délivrée à Lambaréné, le", date, "} \\newline
        \\textcolor{gray}{\\footnotesize{Delivered in Lambaréné, on", date, "}}\\newline \\vspace{2.0cm}
        \\begin{picture}(0, 0)(-1, 55)
        \\includegraphics[height = 2.20cm]{", paste0("signatures/", data_child$signature), "}
        \\end{picture}\\newline
        \\textbf{", data_child$signe_by, "} \\newline
        \\textnormal{} \\newline
    \\end{flushleft}
    \\end{minipage}
\\end{table}"
))

cat("\\vspace{0.30cm}
  \\begin{flushleft}
   \\footnotesize{Devant un résultat négatif, veuillez appeler le 1410 dès l’apparition des signes évocateurs du COVID-19 (fiévre, toux, etc) \\newline
   \\textcolor{gray}{In case of a negative result, please call 1410 as soon as signs suggestive of COVID-19 appear (fever, cough, etc)}}
  \\end{flushleft}"
    )
```

\newpage